package org.jzoie.demo;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.BooleanClause.Occur;
import org.jzoie.ZoieDocument;
import org.jzoie.ZoieQuery;

public class Demo 
{
	public static void main(String[] args) 
	{
		
		//add();
		query();
		//query1();
		//query2();
		//query3();
		System.out.println("finish");
	}
	/**
	 * 添加索引
	 */
	public static void add()
	{
		List<TestEntity> list=new ArrayList<TestEntity>();
		TestEntity entity=new TestEntity();
		entity.setId("1");
		entity.setName("上官飞燕和丹凤");
		entity.setAge(24);
		entity.setSex("女性");
		entity.setExt("2015年度最佳员工");
		list.add(entity);
		
		entity=new TestEntity();
		entity.setId("2");
		entity.setName("庞飞燕与太师");
		entity.setAge(26);
		entity.setSex("女性");
		entity.setExt("cctv花旦");
		list.add(entity);
		
		entity=new TestEntity();
		entity.setId("3");
		entity.setName("燕翻云和凤姐");
		entity.setAge(28);
		entity.setSex("男性");
		entity.setExt("2014n裁判");
		list.add(entity);
		
		ZoieDocument<TestEntity> zoieDocument=new ZoieDocument<TestEntity>(TestEntity.class);
		zoieDocument.add(list);
	}
	/**
	 * 删除索引
	 */
	public static void delete()
	{
		ZoieDocument<TestEntity> zoieDocument=new ZoieDocument<TestEntity>(TestEntity.class);
		zoieDocument.delete("id", "1");
	}
	/**
	 * 更新索引
	 */
	public static void update()
	{
		ZoieDocument<TestEntity> zoieDocument=new ZoieDocument<TestEntity>(TestEntity.class);
		
		TestEntity entity=new TestEntity();
		entity.setName("上官飞燕和丹凤");
		entity.setAge(25);
		entity.setSex("女性");
		
		zoieDocument.update("id", "1",entity);
	}
	/**
	 * 单个字段   "="   查询
	 */
	public static void query()
	{
		ZoieQuery<TestEntity> query=new ZoieQuery<TestEntity>(TestEntity.class);
		List<TestEntity> list=query.query("sex", "男性", true, 0, 11, null, null);
		int allCount=query.getTotal();
		System.out.println("total="+allCount);
		for (int i = 0; i < list.size(); i++)
		{
			System.out.println("------------------------------");
			System.out.println(list.get(i).getName());
			System.out.println(list.get(i).getAge());
			System.out.println(list.get(i).getSex());
			System.out.println(list.get(i).getExt());
		}
		
	}
	/**
	 * 单个字段   "like"   查询
	 */
	public static void query1()
	{
		ZoieQuery<TestEntity> query=new ZoieQuery<TestEntity>(TestEntity.class);
		List<TestEntity> list=query.query("name", "飞燕", true, 0, 11, null, null);
		int allCount=query.getTotal();
		System.out.println("total="+allCount);
		for (int i = 0; i < list.size(); i++)
		{
			System.out.println("------------------------------");
			System.out.println(list.get(i).getName());
			System.out.println(list.get(i).getAge());
			System.out.println(list.get(i).getSex());
		}
	}
	/**
	 * 多个字段   "&"   查询
	 */
	public static void query2()
	{
		ZoieQuery<TestEntity> query=new ZoieQuery<TestEntity>(TestEntity.class);
		String[] keys=new String[]{"飞燕","丹凤"};
		BooleanQuery conditions=new BooleanQuery();
		for (int i = 0; i < keys.length; i++) 
		{
			String fieldValue=keys[i];
			PhraseQuery phraseQuery=new PhraseQuery();
			phraseQuery.setSlop(0);
			for(int j=0;j<fieldValue.length();j++)
			{
				phraseQuery.add(new Term("name",fieldValue.charAt(j)+""));
			}
			conditions.add(phraseQuery,Occur.MUST);
		}
		List<TestEntity> list=query.query(conditions, 0, 11, null, null);
		int allCount=query.getTotal();
		System.out.println("total="+allCount);
		for (int i = 0; i < list.size(); i++)
		{
			System.out.println("------------------------------");
			System.out.println(list.get(i).getName());
			System.out.println(list.get(i).getAge());
			System.out.println(list.get(i).getSex());
		}
	}
	/**
	 * 数字范围 查询
	 */
	public static void query3()
	{
		ZoieQuery<TestEntity> query=new ZoieQuery<TestEntity>(TestEntity.class);
		List<TestEntity> list=query.queryRangeInt("age", 25, 28, true, true, 0, 10, null, null, null);
		int allCount=query.getTotal();
		System.out.println("total="+allCount);
		for (int i = 0; i < list.size(); i++)
		{
			System.out.println("------------------------------");
			System.out.println(list.get(i).getName());
			System.out.println(list.get(i).getAge());
			System.out.println(list.get(i).getSex());
		}
	}
}
