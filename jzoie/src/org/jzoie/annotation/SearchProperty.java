package org.jzoie.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface SearchProperty 
{
	public enum Store{YES,NO};
    Store store() default Store.YES;
    
    public enum Index{ANALYZED,NOT_ANALYZED};
    Index index() default Index.NOT_ANALYZED;
    
    public enum Type{STRING,NUMBER};
    Type type() default Type.STRING;
}
