package org.jzoie.util;

import java.util.HashMap;
import java.util.Map;

public class MemoryMap 
{
	private static Map<String, Long> map=new HashMap<String, Long>();
	public static synchronized void set(String key,Long number)
	{
		map.put(key,number);
	}
	public static synchronized Long get(String key)
	{
		return map.get(key);
	}
}
