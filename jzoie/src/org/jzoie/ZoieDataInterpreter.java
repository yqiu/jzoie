package org.jzoie;

import proj.zoie.api.indexing.ZoieIndexable;
import proj.zoie.api.indexing.ZoieIndexableInterpreter;

public class ZoieDataInterpreter<T> implements ZoieIndexableInterpreter<T>
{
	 public ZoieIndexable interpret(T src)
	  {
	    return new ZoieDataIndex<T>(src);
	  }

	public ZoieIndexable convertAndInterpret(T src) {
		return new ZoieDataIndex<T>(src);
	}

}
