package org.jzoie.sequece;

import org.jzoie.annotation.SearchProperty;
import org.jzoie.annotation.SearchProperty.Index;
import org.jzoie.annotation.SearchProperty.Store;
import org.jzoie.annotation.SearchProperty.Type;

public class Sequece 
{
	private String className="";
	private long count=0;
	@SearchProperty(store=Store.YES,index=Index.NOT_ANALYZED,type=Type.STRING)
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	@SearchProperty(store=Store.YES,index=Index.NOT_ANALYZED,type=Type.NUMBER)
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
}
