package org.jzoie;

import org.apache.lucene.document.Document;
import org.jzoie.util.MemoryMap;
import org.jzoie.util.TransferToDocument;

import proj.zoie.api.indexing.ZoieIndexable;

public class ZoieDataIndex<T> implements ZoieIndexable
{
	private T bean=null;
	private TransferToDocument<T> transfer=new TransferToDocument<T>();
	public ZoieDataIndex() {
	}
	
	public ZoieDataIndex(T bean) {
		this.bean = bean;
	}

	public IndexingReq[] buildIndexingReqs() 
	{
		Document document=transfer.toDocument(bean);
		return new IndexingReq[]{new IndexingReq(document)};
	}

	public byte[] getStoreValue() {
		return null;
	}

	public long getUID() {
		return MemoryMap.get(bean.toString());
	}

	public boolean isDeleted() {
		return false;
		//return  "MARKED_FOR_DELETE".equals(bean.getExt1());
	}
	public boolean isSkip() {
		return false;
	}
	public boolean isStorable() {
		return false;
	}
	
}
