package org.jzoie;

import it.unimi.dsi.fastutil.longs.LongSet;

import java.io.IOException;
import java.util.List;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.TermDocs;
import org.apache.lucene.index.TermPositions;

import proj.zoie.api.ZoieIndexReader;
import proj.zoie.api.indexing.IndexReaderDecorator;

public class ZoieDataIndexReader extends ZoieIndexReader<IndexReader>
{
	public ZoieDataIndexReader(IndexReader in,
			IndexReaderDecorator<IndexReader> decorator) throws IOException {
		super(in, decorator);
	}

	@Override
	public void commitDeletes() {
	}

	@Override
	public ZoieIndexReader<IndexReader> copy() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<IndexReader> getDecoratedReaders() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ZoieIndexReader<IndexReader>[] getSequentialSubReaders() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] getStoredValue(long arg0) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getUID(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected boolean hasIndexDeletions() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isDeleted(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void markDeletes(LongSet arg0, LongSet arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDelDocIds() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public TermDocs termDocs() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TermPositions termPositions() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
	
}
