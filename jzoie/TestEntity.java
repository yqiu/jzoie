package org.jzoie.demo;

import org.jzoie.annotation.SearchProperty;
import org.jzoie.annotation.SearchProperty.Index;
import org.jzoie.annotation.SearchProperty.Store;
import org.jzoie.annotation.SearchProperty.Type;

public class TestEntity 
{
	private String id="";
	private String name="";
	private int age=0;
	private String sex="";
	private String ext="";
	
	@SearchProperty(store=Store.YES,index=Index.NOT_ANALYZED,type=Type.STRING)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@SearchProperty(store=Store.YES,index=Index.ANALYZED,type=Type.STRING)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@SearchProperty(store=Store.YES,index=Index.NOT_ANALYZED,type=Type.NUMBER)
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@SearchProperty(store=Store.YES,index=Index.NOT_ANALYZED,type=Type.STRING)
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	@SearchProperty(store=Store.YES,index=Index.ANALYZED,type=Type.STRING)
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	
}
